if (!require("fs").existsSync("node_modules")) {
	require("child_process").execSync("npm install")
}

const fs = require("fs-extra")

// Copy the public folder to netlify's working folder
fs.removeSync("public")
fs.copySync("../../dist", "public")
