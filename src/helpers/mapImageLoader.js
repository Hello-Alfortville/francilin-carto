let pixelRatio = Math.min(2, window.devicePixelRatio)

async function loadImage(map, url, name) {
	return new Promise((resolve, reject) => {
		map.loadImage(url, function (error, image) {
			if (error) {
				reject(error)
			}
			if (!map.hasImage(name)) {
				map.addImage(name, image)
			}
			resolve()
		})
	})
}

export async function loadMapImages(map) {
	let suffix = pixelRatio < 2 ? "" : "_hd"
	let images = []
	for (let i = 1; i <= 8; i++) {
		images.push(loadImage(map, `/marker-icons/marker_${i}${suffix}.png`, `${i}`))
	}
	images.push(loadImage(map, "/marker-icons/cluster/marker.png", "marker-cluster"))
	return Promise.all(images)
}

export function handleImageMissing({ id, target: map }) {
	let url
	if (id === "marker-cluster") {
		url = "/marker-icons/cluster/marker.png"
	} else {
		let suffix = pixelRatio < 2 ? "" : "_hd"
		url = `/marker-icons/marker_${id}${suffix}.png`
	}
	loadImage(map, url, id)
}
