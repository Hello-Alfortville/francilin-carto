const mainThemePropertyName = "thematique_principale"
let pixelRatio = Math.min(2, window.devicePixelRatio)

export const iconImageExpression = ["get", mainThemePropertyName]

export const clusterLayerFilter = ["has", "point_count"]
export const clusterLayerLayout = {
	"icon-image": "marker-cluster",
	"icon-allow-overlap": true,
	"icon-size": ["step", ["get", "point_count"], 0.35, 10, 0.42, 30, 0.5, 50, 0.6, 100, 0.7],
	"text-field": [
		"case",
		[">=", ["get", "point_count"], 200],
		"200+",
		[">=", ["get", "point_count"], 100],
		"100+",
		[">=", ["get", "point_count"], 50],
		"50+",
		[">=", ["get", "point_count"], 30],
		"30+",
		[">=", ["get", "point_count"], 10],
		"10+",
		["get", "point_count"],
	],
	"text-font": ["Open Sans Bold", "Arial Unicode MS Bold"],
	"text-size": ["step", ["get", "point_count"], 13, 10, 15, 30, 17, 50, 19, 100, 22],
}

export let symbolLayerFilter = ["!", ["has", "point_count"]]
export const symbolLayerLayout = {
	"icon-image": iconImageExpression, // overwritten by applyFilters
	"icon-size": pixelRatio < 2 ? 1 : 0.5,
	"icon-allow-overlap": true, // the icon will be visible even if it collides with other previously drawn symbols
}
