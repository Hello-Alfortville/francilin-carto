import * as aideIcons from "~/assets/aideIcons"
import * as themeIcons from "~/assets/themeIcons"
import * as urgenceIcons from "~/assets/urgenceIcons"
import * as lieuIcons from "~/assets/lieuIcons"

export function getStepIconData(filter, $filters) {
	let iconName
	switch (filter) {
		case "theme":
			iconName = $filters[filter] === null ? `${filter}_default` : `${filter}_${$filters[filter]}`
			return themeIcons[iconName]
		case "urgence":
			iconName = $filters[filter] === null ? `${filter}_default` : `${filter}_${$filters[filter]}`
			return urgenceIcons[iconName]
		case "aide":
			iconName = $filters["aide"] === null ? `${filter}_default` : `${filter}_${$filters[filter].accompagnement}`
			return aideIcons[iconName]
		case "lieu":
			iconName = filter
			// iconName = $mapLocation === null ? `${filter}_default` : filter
			return lieuIcons[iconName]
	}
}
