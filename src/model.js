import SimpleOpeningHours from "simple-opening-hours"

export function isOpenNow(feature) {
	const openingHoursProperty = feature.properties.horaires

	if (!openingHoursProperty) {
		return null
	}
	const openingHours = new SimpleOpeningHours(openingHoursProperty)
	return openingHours.isOpen()
}

export function formatSiret(value) {
	return [value.slice(0, 3), value.slice(3, 6), value.slice(6, 9), value.slice(10)].join(" ")
}

export function normalizeProperties(properties) {
	return {
		francilin_id: properties.francilin_id || null,
		SIRET: properties.SIRET || null,
		lieu_nom: properties.lieu_nom || null,
		contact_nom: properties.contact_nom || null,
		adresse: properties.adresse || null,
		code_postal: properties.code_postal || null,
		commune: properties.commune || null,
		e_mail: properties.e_mail || null,
		telephone: properties.telephone || null,
		thematique_principale: properties.thematique_principale || null,
		site_web: properties.site_web || null,
		horaires: properties.horaires || null,
		aide: properties.aide || [],
		accessibilite: properties.accessibilite || null,
		services: properties.services || [],
		label: properties.label || [],
		mise_a_jour: properties.mise_a_jour || [],
	}
}

export function findFeatureById(structures, featureId) {
	return structures.features.find((feature) => featureId === feature.id)
}

export function hasFeatures(structures) {
	return structures.features && structures.features.length > 0
}

// Theme - service ids relationship
//
// Theme ids are 1-digit string, e.g.  "3", "6", ...
// Service ids are 3-digit string, e.g. "136", "203", ...
// First service id digit is the theme id
export function belongToTheme(serviceId, themeId) {
	return serviceId.startsWith(themeId)
}

export function themeIdFromServiceId(serviceId) {
	return serviceId[0]
}
