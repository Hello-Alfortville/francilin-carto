export { default as urgence_false } from "./urgence-icons/urgence_false.svg"
export { default as urgence_true } from "./urgence-icons/urgence_true.svg"
export { default as urgence_default } from "./urgence-icons/urgence_default.svg"
