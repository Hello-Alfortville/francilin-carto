const defaultTheme = require("tailwindcss/resolveConfig")(require("tailwindcss/defaultConfig")).theme
const { spacing } = defaultTheme

module.exports = {
	future: {
		removeDeprecatedGapUtilities: true,
		purgeLayersByDefault: true,
	},
	purge: ["public/index.html", "src/**/*.svelte"],
	theme: {
		colors: {
			gray: {
				// 100: "#f2f2f2",
				// shadow: "rgba(0, 0, 0, .1)",
				// 200: "#C3C3C3", // only for selected localePicker
				// // 300: "#AAAAAA",
				// 400: "#919191", // only for btn-outline disabled
				// 500: "#777777",
				// // 600: "#5E5E5E",
				// 700: "#444444",
				// 800: "#2A2A2A",
				// // 900: "#111111",

				"100": "#f4f5f7",
				"200": "#e5e5e5",
				// "300": "#d2d6dc",
				"300": "#cbd5e0",
				// "350": "#B9BEC7",
				"400": "#9fa6b2",
				"500": "#718096",
				"600": "#4b5563",
				"700": "#374151",
				"800": "#252f3f",
				"900": "#1e1e1c",
			},
			orange: {
				// 100: "#FDE8D8",
				// 200: "#FBCBA7",
				// 300: "#F9AE76",
				// 400: "#F69146",
				500: "#F47415",
				600: "#CC5D0A",
				// 700: "#9C4707",
				// 800: "#6B3005",
				// 900: "#3A1A03",
			},
			blue: {
				// 100: "#7AB3FF",
				// 200: "#4796FF",
				// 300: "#1479FF",
				// 400: "#0060E0",
				500: "#004aad",
				// 600: "#00347A",
				// 700: "#001E47",
				// 800: "#000914",
				// 900: "#000000",
			},
			pink: {
				100: "#F6E0E5",
				200: "#F0CCD4",
				// 300: "#E4A5B3",
				// 400: "#D97D91",
				500: "#cd5670",
				// 600: "#B93753",
				700: "#922B42",
				// 800: "#6A2030",
				// 900: "#43141E",
			},
			black: "#000000",
			white: "#ffffff",
			mapbox: {
				shadow: "rgba(0, 0, 0, .1)",
			},
		},

		extend: {
			screens: {
				print: { raw: "print" },
			},
			boxShadow: {
				mapbox: "0 0 0 2px rgba(0,0,0,.1)",
				pink: "0 0 0 2px #cd5670",
			},
		},
		customForms: (theme) => ({
			default: {
				input: {
					borderColor: theme("colors.gray.800"),
					borderWidth: theme("borderWidth.2"),
				},
				checkbox: {
					width: spacing[5],
					height: spacing[5],
					color: theme("colors.gray.700"),
					transition: "border-color 0.2s, background-color 0.2s",
					cursor: "pointer",
					"&:focus": {
						outline: "none",
						"box-shadow": "0 0 0 3px" + theme("colors.gray.400"),
						"border-color": "transparent",
					},
				},
			},
		}),
		boxShadow: (theme) => ({
			inner: "inset 0 0 0 2px " + theme("colors.gray.700"),
		}),
	},
	variants: {},
	plugins: [
		require("@tailwindcss/custom-forms"), //
		require("tailwindcss-rtl"),
	],
}
